package com.example.poleiskrieg.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.LoadGameViewModel;

public class LoadGameActivity extends AppCompatActivity {

    private LoadGameViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_game);
        this.model = new ViewModelProvider(this).get(LoadGameViewModel.class);
    }


}