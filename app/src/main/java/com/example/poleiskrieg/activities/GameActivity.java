package com.example.poleiskrieg.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;
import com.example.poleiskrieg.viewmodel.Factories.GameViewModelFactory;

public class GameActivity extends AppCompatActivity {

    GameViewModel model;
    TutorialViewModel tutorialViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long gameId = getIntent().getLongExtra(C.GAME_ID, -1);
        ClassScan.setConfigurationFile(R.raw.scan_classes, this);
        tutorialViewModel = new ViewModelProvider(this).get(TutorialViewModel.class);
        model = new ViewModelProvider(this, new GameViewModelFactory(getApplication(), gameId, -1)).get(GameViewModel.class);
        setContentView(R.layout.activity_game);
        //TODO on button back, if dialog opened, exits, else go to main menu
    }

    @Override
    public void onBackPressed() {
        if (tutorialViewModel.getIsTutorialMenuVisible().getValue()) {
            tutorialViewModel.setIsTutorialMenuVisible(false);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
        }
    }

    //TODO come rifinitura finale implementare onSaveSTate e onActivityRestored per salvare lo stato dell'interfaccia grafica, nel caso in cui l'activity si arresti per system initiated process kill
}
