package com.example.poleiskrieg.activities;

import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.network.NetworkCallbacksFactory;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

public class OnlineLoadGameActivity extends AbstractOnlineActivity {

    private OnlineLoadGameViewModel model;
    private ConnectivityManager.NetworkCallback connectionLostCallback;
    private NetworkManager networkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_menu_activity);
        this.model = new ViewModelProvider(this).get(OnlineLoadGameViewModel.class);
    }

    @Override
    public void onBackPressed() {
        model.setIsJoinVisible(false);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
