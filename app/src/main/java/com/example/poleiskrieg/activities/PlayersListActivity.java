package com.example.poleiskrieg.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;
import com.example.poleiskrieg.viewmodel.Factories.PlayerListViewModelFactory;
import com.example.poleiskrieg.viewmodel.playerlist.PlayersListViewModel;

public class PlayersListActivity extends AppCompatActivity {

    private PlayersListViewModel model;
    private long gameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_list);
        ClassScan.setConfigurationFile(R.raw.scan_classes, this);

        this.gameId = getIntent().getLongExtra(C.GAME_ID, -1);
        this.model =  new ViewModelProvider(this, new PlayerListViewModelFactory(getApplication(), gameId)).get(PlayersListViewModel.class);
    }
    @Override
    protected void onStop() {
        super.onStop();
        //stopService(new Intent(this, SoundService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //stopService(new Intent(this, SoundService.class));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoadGameActivity.class);
        this.startActivity(intent);
    }
}