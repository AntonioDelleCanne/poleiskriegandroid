package com.example.poleiskrieg.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.Factories.OnlineSetupGameViewModelFactory;
import com.example.poleiskrieg.viewmodel.setupgameviewmodel.OnlineSetupGameViewModel;

public class SetupOnlineGameActivity extends SetupGameActivity {

    @Override
    protected void initializeModel() {
        this.model = new ViewModelProvider(this, new OnlineSetupGameViewModelFactory(getApplication(), findViewById(R.id.gameEditText))).get(OnlineSetupGameViewModel.class);
    }

    @Override
    protected void setSelectButtonListener() {
        findViewById(R.id.selectBtn).setOnClickListener(view -> {
            LiveData<Long> gameId = this.model.createGame();
            gameId.observe(this, aLong -> {
                if (aLong != -1) {
                    Intent intent = new Intent(view.getContext(), OnlinePlayerListActivity.class);
                    intent.putExtra(C.GAME_ID, gameId.getValue());
                    view.getContext().startActivity(intent);
                }
            });
        });
    }
}
