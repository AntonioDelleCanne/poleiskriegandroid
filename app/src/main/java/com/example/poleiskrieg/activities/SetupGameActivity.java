package com.example.poleiskrieg.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.sounds.SoundService;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;
import com.example.poleiskrieg.viewmodel.Factories.SetupGameViewModelFactory;
import com.example.poleiskrieg.viewmodel.setupgameviewmodel.OfflineSetupGameViewModel;
import com.example.poleiskrieg.viewmodel.setupgameviewmodel.SetupGameViewModel;

public class SetupGameActivity  extends AppCompatActivity {

    protected SetupGameViewModel model;
    private TextView players;
    private TextView selectedMode;
    private TextView modeDescription;
    private int count;
    private LiveData<Integer> playersNum;
    private LiveData<GameRules> gameMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_game_mode);
        ClassScan.setConfigurationFile(R.raw.scan_classes, this);
        this.players = findViewById(R.id.numberText);
        this.selectedMode = findViewById(R.id.selectedMode);
        this.modeDescription = findViewById(R.id.modeDescription);
        this.initializeModel();
        this.count = 0;

        this.playersNum = model.getPlayersNum();
        this.playersNum.observe(this, integer -> {
            findViewById(R.id.incrementBtn).setVisibility(View.VISIBLE);
            findViewById(R.id.decraseBtn).setVisibility(View.VISIBLE);
            if (integer == this.model.getMaximumPlayers()){
                findViewById(R.id.incrementBtn).setVisibility(View.INVISIBLE);
            }
            if (integer == this.model.getMinimumPlayers()){
                findViewById(R.id.decraseBtn).setVisibility(View.INVISIBLE);
            }
            this.players.setText(String.valueOf(integer));
        });

        this.gameMode = model.getGameMode();
        this.gameMode.observe(this, gameRules -> {
            this.selectedMode.setText(this.model.getActualGameModeNameId());
            this.modeDescription.setText(this.model.getActualGameModeDescriptionId());
        });

        setUI();
    }

    protected void initializeModel(){
        this.model = new ViewModelProvider(this, new SetupGameViewModelFactory(getApplication(), findViewById(R.id.gameEditText))).get(OfflineSetupGameViewModel.class);
    }

    protected void setUI(){
        findViewById(R.id.decraseBtn).setOnClickListener(view->{
            C.doClickFeeling(view);
            this.model.changePlayersNum(false);
        });
        findViewById(R.id.incrementBtn).setOnClickListener(view->{
            C.doClickFeeling(view);
            this.model.changePlayersNum(true);
        });
        findViewById(R.id.leftBtn).setOnClickListener(view->{
            C.doClickFeeling(view);
            this.model.setNewGameMode(false);
        });
        findViewById(R.id.rightBtn).setOnClickListener(view->{
            C.doClickFeeling(view);
            this.model.setNewGameMode(true);
        });
        this.setSelectButtonListener();
    }

    protected void setSelectButtonListener() {
        findViewById(R.id.selectBtn).setOnClickListener(view -> {
            LiveData<Long> gameId = this.model.createGame();
            gameId.observe(this, aLong -> {
                if (aLong != -1) {
                    Intent intent = new Intent(view.getContext(), PlayersListActivity.class);
                    intent.putExtra(C.GAME_ID, gameId.getValue());
                    view.getContext().startActivity(intent);
                }
            });
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, SoundService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, SoundService.class));
    }


}