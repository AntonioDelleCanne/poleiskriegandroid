package com.example.poleiskrieg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.util.C;

import java.util.List;

public class SaveStateElementAdapter extends RecyclerView.Adapter<SaveStateElementAdapter.AdapterViewHolder> implements ImageWithTextAdapter.OnButtonClickListener{

    private final LiveData<List<GameWithPlayers>> listGameWithPlayers;
    private SaveStateElementAdapter.OnButtonClickListener onButtonClickListener;

    public SaveStateElementAdapter(LiveData<List<GameWithPlayers>> listGameWithPlayers, SaveStateElementAdapter.OnButtonClickListener onButtonClickListener) {
        this.listGameWithPlayers = listGameWithPlayers;
        this.onButtonClickListener = onButtonClickListener;
    }

    @Override
    public SaveStateElementAdapter.AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_savedata, parent, false);
        return new SaveStateElementAdapter.AdapterViewHolder(view, this.onButtonClickListener);
    }

    @Override
    public void onBindViewHolder(SaveStateElementAdapter.AdapterViewHolder holder, int position) {
        GameWithPlayers currentItem = listGameWithPlayers.getValue().get(position);
        PlayerEntity player = currentItem.playerEntityList.stream().filter(p -> p.id == currentItem.gameEntity.turnPlayerId).findFirst().get();
        holder.text.setText(currentItem.gameEntity.gameName);
        holder.turnInfo.setText(player.name);
        //holder.infoButton =
    }

    @Override
    public int getItemCount() {
        return listGameWithPlayers.getValue().size();
    }

    @Override
    public void onButtonClick(int position) {

    }

    /** AdapterViewHolder class*/
    public static class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView text;
        private TextView turnInfo;
        private ButtonWithFeeling infoButton;
        private ButtonWithFeeling loadButton;
        private ButtonWithFeeling deleteButton;
        SaveStateElementAdapter.OnButtonClickListener onButtonClickListener;

        public AdapterViewHolder(View itemView, SaveStateElementAdapter.OnButtonClickListener onButtonClickListener) {
            super(itemView);
            this.text = itemView.findViewById(R.id.saveDataName);
            this.turnInfo = itemView.findViewById(R.id.playerInfoTextView);
            this.infoButton = itemView.findViewById(R.id.turnButton);
            this.loadButton = itemView.findViewById(R.id.loadButton);
            this.deleteButton = itemView.findViewById(R.id.deleteButton);
            infoButton.setOnClickListener(this);
            loadButton.setOnClickListener(this);
            deleteButton.setOnClickListener(this);
            this.onButtonClickListener = onButtonClickListener;
        }

        @Override
        public void onClick(View view) {
            this.onButtonClickListener.onButtonClick(getAdapterPosition(), view.getId());
        }
    }

    /** OnButtonClickListener interface*/

    public interface OnButtonClickListener{
        void onButtonClick(int position, int buttonId);
    }
}
