package com.example.poleiskrieg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.element.ButtonTextImage;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.util.C;

import java.util.ArrayList;

public class ButtonTextImageAdapterGrid<X> extends RecyclerView.Adapter<ButtonTextImageAdapterGrid.AdapterViewHolder>{

    private ArrayList<ButtonTextImage<X>> list;
    private OnButtonClickListener onButtonClickListener;

    public ButtonTextImageAdapterGrid(ArrayList<ButtonTextImage<X>> list, OnButtonClickListener onButtonClickListener) {
        this.list = list;
        this.onButtonClickListener = onButtonClickListener;
    }

    public void setList(ArrayList<ButtonTextImage<X>> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_button_text_image_grid, parent, false);
        return new AdapterViewHolder(view, this.onButtonClickListener);
    }

    @Override
    public void onBindViewHolder(AdapterViewHolder holder, int position) {
        ButtonTextImage<X> currentItem = list.get(position);
        if (currentItem.getImageDirectionResources() == C.LEFT){
            holder.button.setCompoundDrawablesWithIntrinsicBounds(currentItem.getImageResource(), 0, 0, 0);
        } else if (currentItem.getImageDirectionResources() == C.TOP){
            holder.button.setCompoundDrawablesWithIntrinsicBounds(0, currentItem.getImageResource(), 0, 0);
        } else if (currentItem.getImageDirectionResources() == C.RIGHT){
            holder.button.setCompoundDrawablesWithIntrinsicBounds(0, 0, currentItem.getImageResource(), 0);
        } else if (currentItem.getImageDirectionResources() == C.BOTTOM){
            holder.button.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, currentItem.getImageResource());
        }
        if(currentItem.getTextResource() instanceof Integer){
            holder.button.setText((Integer)currentItem.getTextResource());
        }
        if(currentItem.getTextResource() instanceof String){
            holder.button.setText((String)currentItem.getTextResource());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    /** AdapterViewHolder class*/
    public static class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ButtonWithFeeling button;
        OnButtonClickListener onButtonClickListener;

        private AdapterViewHolder(View itemView, OnButtonClickListener onButtonClickListener) {
            super(itemView);
            this.button = itemView.findViewById(R.id.turnButton);
            this.onButtonClickListener = onButtonClickListener;
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.onButtonClickListener.onButtonClick(getAdapterPosition());
        }
    }

    /** OnButtonClickListener interface*/

    public interface OnButtonClickListener{
        void onButtonClick(int position);
    }
}
