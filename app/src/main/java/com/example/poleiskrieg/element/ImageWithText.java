package com.example.poleiskrieg.element;

public class ImageWithText<X> {
    private int image;
    private X textId;

    public ImageWithText(int image, X textId) {
        this.image = image;
        this.textId = textId;
    }

    public int getImageResource() {
        return this.image;
    }

    public X getTextResource() {
        return textId;
    }
}
