package com.example.poleiskrieg.element;

import android.content.Context;
import android.util.AttributeSet;

import com.example.poleiskrieg.util.C;

public class ImageButtonWithFeeling extends androidx.appcompat.widget.AppCompatImageButton {

    public ImageButtonWithFeeling(Context context) {
        super(context);
        this.setOnClickListener(C::doClickFeeling);
    }

    public ImageButtonWithFeeling(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnClickListener(C::doClickFeeling);
    }

    public ImageButtonWithFeeling(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setOnClickListener(C::doClickFeeling);
    }
}
