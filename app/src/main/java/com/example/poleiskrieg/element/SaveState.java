package com.example.poleiskrieg.element;


public class SaveState {
    private int nameText;


    public SaveState(int nameText) {
        this.nameText = nameText;
    }

    public int getNameTextResource() {
        return this.nameText;
    }

}

