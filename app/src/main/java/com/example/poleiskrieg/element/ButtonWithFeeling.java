package com.example.poleiskrieg.element;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;

public class ButtonWithFeeling extends androidx.appcompat.widget.AppCompatButton {


    public ButtonWithFeeling(Context context) {
        super(context);
        initButton();
    }

    public ButtonWithFeeling(Context context, AttributeSet attrs) {
        super(context, attrs);
        initButton();
    }

    public ButtonWithFeeling(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initButton();
    }

    private void initButton(){
        this.setOnClickListener(C::doClickFeeling);
    }
}
