package com.example.poleiskrieg.database.converters;

import androidx.room.TypeConverter;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.example.poleiskrieg.logics.game.model.resources.BasicResources;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import java.util.Date;

public class TypeConverters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static String raceToName(Race race) {
        return race == null ? null : race.getClass().getName();
    }

    @TypeConverter
    public static Race nameToRace(String name) {
        try {
            return name == null ? null : (Race) Class.forName(name).newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @TypeConverter
    public static String gameRulesToName(GameRules gameRules) {
        return gameRules == null ? null : gameRules.getClass().getName();
    }

    @TypeConverter
    public static GameRules nameToGameRules(String name) {
        try {
            return name == null ? null : (GameRules) Class.forName(name).newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @TypeConverter
    public static String objectiveRulesToName(Objective objective) {
        return objective == null ? null : objective.getClass().getName();
    }

    @TypeConverter
    public static Objective nameToObjective(String name) {
        try {
            return name == null ? null : (Objective) Class.forName(name).newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @TypeConverter
    public static String terrainToName(Terrain terrain) {
        return terrain == null ? null : terrain.getClass().getName();
    }

    @TypeConverter
    public static Terrain nameToTerrain(String name) {
        try {
            return name == null ? null : (Terrain) Class.forName(name).newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @TypeConverter
    public static String resourceToName(Resource resource) {
        return resource == null ? null : ((BasicResources)resource).name();
    }

    @TypeConverter
    public static Resource nameToResource(String name) {
        return name == null ? null : BasicResources.valueOf(name);
    }


}
