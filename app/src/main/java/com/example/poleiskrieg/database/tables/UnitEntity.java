package com.example.poleiskrieg.database.tables;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(primaryKeys = {"gameId", "x", "y", "isVehicle"})
public class UnitEntity {
    public long gameId;
    public int x;
    public int y;
    public boolean isVehicle; //in a case can be a vehicle and a unit at the same time, if so, the unit is the passenger

    @ColumnInfo(defaultValue = "-1")
    public int ownerPlayerId; //if -1 neutral
    public String className;
    public int hp;
    public int attackCount;
    public boolean didFirstMovement;
    public boolean movedAfterAttack;

    public UnitEntity(){
        ownerPlayerId = -1;
    }
}
