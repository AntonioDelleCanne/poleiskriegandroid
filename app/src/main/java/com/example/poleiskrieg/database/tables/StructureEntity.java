package com.example.poleiskrieg.database.tables;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(primaryKeys = {"gameId", "x", "y"})
public class StructureEntity {
    public long gameId;
    public int x;
    public int y;


    public String className; //check isSubclass to implement
    @ColumnInfo(defaultValue = "-1")
    public int produced; //-1 if null
    @ColumnInfo(defaultValue = "-1")
    public int remaining; //-1 if null
    @ColumnInfo(defaultValue = "-1")
    public int ownerPlayerId; //-1 if empty
    @ColumnInfo(defaultValue = "-1")
    public int conquerorPlayerId; //-1 if empty, city doesn't have left and produced, harbor has all to -1

    public StructureEntity(){
        produced = -1;
        remaining = -1;
        ownerPlayerId = -1;
        conquerorPlayerId = -1;
    }
}
