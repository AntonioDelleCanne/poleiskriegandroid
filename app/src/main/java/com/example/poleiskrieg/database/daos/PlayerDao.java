package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;

import java.util.Date;
import java.util.List;

@Dao
public abstract class PlayerDao extends BaseDao<PlayerEntity>{

    @Query("UPDATE PlayerEntity SET eliminated = :eliminated WHERE id = :id AND gameId = :gameId")
    public abstract int update(int id, long gameId, boolean eliminated);

    @Transaction
    @Query("SELECT * FROM PlayerEntity WHERE gameId = :gameId AND id =:id")
    public abstract PlayerEntity getById(long gameId, int id);

    @Transaction
    @Query("SELECT * FROM PlayerEntity WHERE gameId = :gameId")
    public abstract List<PlayerEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM PlayerEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}
