package com.example.poleiskrieg.database.tables;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.util.C;

import java.util.Date;

@Entity
public class GameEntity {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(defaultValue = "-1")
    public long onlineId; //-1 if offline
    public String gameName;
    public int turnPlayerId;
    public int mapHeight;
    public int numberOfPlayers;
    public int mapWidth;
    public GameRules gameRules;
    public boolean firstTurn;
    public boolean isValid;
    public Date dateSaved; //used to synchronize online players and to be shown in the load menu
    public String creatorId; //-1 if offline

    public GameEntity(){
        onlineId = -1;
        creatorId = C.NOT_LOGGED_IN;
    }
}
