package com.example.poleiskrieg.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.poleiskrieg.database.converters.TypeConverters;
import com.example.poleiskrieg.database.daos.AccountDao;
import com.example.poleiskrieg.database.daos.CaseDao;
import com.example.poleiskrieg.database.daos.GameDao;
import com.example.poleiskrieg.database.daos.PlayerDao;
import com.example.poleiskrieg.database.daos.PlayerResourceDao;
import com.example.poleiskrieg.database.daos.SkillTreeAttributeDao;
import com.example.poleiskrieg.database.daos.StructureDao;
import com.example.poleiskrieg.database.daos.UnitDao;
import com.example.poleiskrieg.database.tables.AccountEntity;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Database(version = 1, entities = {CaseEntity.class, GameEntity.class, PlayerEntity.class, ResourceEntity.class, SkillTreeAttributeEntity.class, StructureEntity.class, UnitEntity.class, AccountEntity.class})
@androidx.room.TypeConverters({TypeConverters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static volatile AppDatabase INSTANCE;
    private static final String DB_NAME = "app_database.db";
    private static final int NUMBER_OF_THREADS = 1;

    //ExecutorService with a fixed thread pool that you will use to run database operations
    // asynchronously on a background thread.
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract PlayerDao getPlayerDao();
    public abstract GameDao getGameDao();
    public abstract PlayerResourceDao getPlayerResourceDao();
    public abstract SkillTreeAttributeDao getSkillTreeAttributeDao();
    public abstract StructureDao getStructureDao();
    public abstract CaseDao getCaseDao();
    public abstract UnitDao getUnitDao();
    public abstract AccountDao getAccountDao();

    /**
     * It'll create the database the first time it's accessed, using Room's database
     * @param context the context of the Application
     * @return the singleton
     */
    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DB_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
