package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;

import java.util.List;


@Dao
public abstract class UnitDao extends BaseDao<UnitEntity> {
    @Transaction
    @Query("SELECT * FROM UnitEntity WHERE gameId = :gameId AND x = :x AND y = :y")
    public abstract UnitEntity getById(long gameId, int x, int y);

    @Transaction
    @Query("SELECT * FROM UnitEntity WHERE gameId = :gameId")
    public abstract List<UnitEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM UnitEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}
