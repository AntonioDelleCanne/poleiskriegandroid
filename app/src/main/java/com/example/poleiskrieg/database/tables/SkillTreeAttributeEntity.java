package com.example.poleiskrieg.database.tables;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(primaryKeys = {"playerOwnerId", "gameId", "className"})
public class SkillTreeAttributeEntity {
    public int playerOwnerId;
    public long gameId;
    public @NonNull String className;

    public int currentLevel;
}
