package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import java.util.List;

@Dao
public abstract class SkillTreeAttributeDao extends BaseDao<SkillTreeAttributeEntity> {
    @Transaction
    @Query("SELECT * FROM SkillTreeAttributeEntity WHERE gameId = :gameId AND playerOwnerId = :playerOwnerId AND className = :className")
    public abstract SkillTreeAttributeEntity getById(long gameId, int playerOwnerId, String className);

    @Transaction
    @Query("SELECT * FROM SkillTreeAttributeEntity WHERE gameId = :gameId AND playerOwnerId = :playerOwnerId")
    public abstract List<SkillTreeAttributeEntity> getByPlayer(long gameId, int playerOwnerId);

    @Transaction
    @Query("SELECT * FROM SkillTreeAttributeEntity WHERE gameId = :gameId")
    public abstract List<SkillTreeAttributeEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM SkillTreeAttributeEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}
