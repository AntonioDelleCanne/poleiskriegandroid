package com.example.poleiskrieg.logics.game.controller;

/**
 * An interface that models an object that can be updated.
 */
public interface Updater {

    /**
     * called to update.
     */
    void update();
}
