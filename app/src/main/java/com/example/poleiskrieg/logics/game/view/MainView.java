package com.example.poleiskrieg.logics.game.view;

import com.example.poleiskrieg.logics.game.controller.Updater;

/**
 * The main view that links all the single views.
 */
public interface MainView extends Updater {

    /**
     * Closes the graphical view and with it the entire application.
     */
    void exit();
}
