package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.Capital;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The CapitalLevel class extends LevelAttribute with Capital as generic type.
 * This class manage the capital level structure. A player can increase the
 * level of his capital.
 */
public class CapitalLevel extends LevelAttribute<Capital> {

    private static final int ATTRIBUTE_NAME = R.string.skilltree_attribute_capital;
    private static final String WHITELIST_PACKAGE = "com.example.poleiskrieg.logics.game.model.objects.structures";
    private static final String CLASSES_IMPLEMENTING = "com.example.poleiskrieg.logics.game.model.objects.structures.Capital";
    private static final String FILTER_START = "com.example.poleiskrieg.logics.game.model.objects.structures.Level";
    private static final String FILTER_END = "Capital";
    private static final int INITIAL_LEVEL = 0;

    /**
     * UnitLevel constructor.
     */
    public CapitalLevel() {
        super(INITIAL_LEVEL, WHITELIST_PACKAGE, CLASSES_IMPLEMENTING, FILTER_START, FILTER_END);
    }

    /** {@inheritDoc} **/
    @Override
    protected Optional<Capital> getObject(final int level) {
        return getCapitalWithOwner(level, Optional.empty());
    }

    @Override
    public int getAttributeNameId() {
        return this.ATTRIBUTE_NAME;
    }

    /** {@inheritDoc} **/
    @Override
    public Cost getCost() {
        return getNewObject().get().getUnlockCost();
    }

    /**
     * This method can be use to get the capital of the actual level.
     * @param owner is the owner of the SkillTree that has this attribute.
     * @return the actual level capital.
     */
    public Capital getActualCapital(final Player owner) {
        return getCapitalWithOwner(getCurrentValue(), Optional.of(owner)).get();
    }

    private Optional<Capital> getCapitalWithOwner(final int level, final Optional<Player> owner) {
        try {
            return Optional.of((Capital) Class.forName(getObjectClasses().get(level)).getConstructor(Optional.class)
                    .newInstance(owner));
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException | ClassNotFoundException | IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttributeLevelNameId() {
        return getNewObject().get().getNameId();
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) {
        Map<Integer, Optional<String>> descriptionMap = new LinkedHashMap<>();
        descriptionMap.put(R.string.structure_info_population, Optional.of(Integer.toString(getNewObject().get().getProducedQuantity())));
        descriptionMap.put(R.string.structure_info_attack_reduction, Optional.of((100 - getNewObject().get().getEnemyAttackReduction() * 100) + "%"));
        return descriptionMap;
    }

}
