package com.example.poleiskrieg.logics.game.model.objects.unit;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.abilities.Ability;
import com.example.poleiskrieg.logics.game.model.abilities.BasicAbilities;
import com.example.poleiskrieg.logics.game.model.objects.AbstractGameObject;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * The UnitImpl class extends AbstractGameObject and implements Unit. It
 * represent a GameObject that can be created, moved and that can fight with
 * other unit. The units have particular attributes. Each unit has a name, an
 * unit type, a strength, a hp, a movement range, an attack range, a cost, an
 * unlock cost and some abilities. When a player create an unit, in this turn,
 * the unit can't be move. Every unit can be move one time par turn, but there
 * are some unit that can also do a second movement after an attack. If an unit
 * attack before move, then i can't do its first movement. If an unit that can
 * move after and that has more then one possible attack par turn, do an attack
 * and then move, then it can't do the other attack. Could be some units that,
 * after killing an enemy, move in enemy position.
 *
 */
public class UnitImpl extends AbstractGameObject implements Unit {

    private final int nameId;
    private final int strength;
    private final int initialHp;
    private int hp;
    private final int movementRange;
    private final int attackRange;
    private final int possibleAttack;
    private int attackCount;
    private boolean didFirstMovement;
    private boolean movedAfterAttack;
    private final boolean canMoveAfterAttack;
    private final boolean moveOnKill;
    private final UnitType unitType;
    private final Cost unitCost;
    private final Cost unitUnlockCost;
    private final Set<Ability> abilities;

    /**
     * UnitImpl constructor with owner.
     * 
     * @param owner              unit's owner. It the Optional is empty it means
     *                           that the unit doesn't have an owner.
     * @param nameId             unit's name id.
     * @param strength           unit's strength.
     * @param initialHp          unit's initial life.
     * @param movementRange      unit's movement range.
     * @param attackRange        unit's attack range.
     * @param possibleAttack     unit's possible attack in a turn.
     * @param canMoveAfterAttack if the unit can move after he has attacked someone.
     * @param moveOnKill         if the unit kills the opponent, he moves in the
     *                           opponent's cell.
     * @param unitCost           unit's cost.
     * @param unitUnlockCost     unit's unlock cost.
     * @param unitType           unit's type.
     */
    public UnitImpl(final Optional<Player> owner, final int nameId, final int strength, final int initialHp,
                    final int movementRange, final int attackRange, final int possibleAttack, final boolean canMoveAfterAttack,
                    final boolean moveOnKill, final Cost unitCost, final Cost unitUnlockCost, final UnitType unitType) {
        set(owner);
        this.nameId = nameId;
        this.strength = strength;
        this.initialHp = initialHp;
        this.hp = initialHp;
        this.movementRange = movementRange;
        this.attackRange = attackRange;
        this.possibleAttack = possibleAttack;
        this.canMoveAfterAttack = canMoveAfterAttack;
        this.moveOnKill = moveOnKill;
        this.unitCost = unitCost;
        this.unitUnlockCost = unitUnlockCost;
        this.movedAfterAttack = true;
        this.didFirstMovement = true;
        this.unitType = unitType;
        this.attackCount = possibleAttack;
        this.abilities = new HashSet<>();
        this.abilities.add(BasicAbilities.WALKONLAND);
    }

    /** {@inheritDoc} **/
    @Override
    public UnitType getUnitType() {
        return unitType;
    }

    /** {@inheritDoc} **/
    @Override
    public int getStrength() {
        return this.strength;
    }

    /** {@inheritDoc} **/
    @Override
    public void setHp(final int hp) {
        this.hp = hp;
    }

    /** {@inheritDoc} **/
    @Override
    public int getHp() {
        return this.hp;
    }

    /** {@inheritDoc} **/
    @Override
    public int getInitialHp() {
        return this.initialHp;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean isDead() {
        return this.hp <= 0;
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttackRange() {
        return this.attackRange;
    }

    /** {@inheritDoc} **/
    @Override
    public int getMovementRange() {
        return this.movementRange;
    }

    /** {@inheritDoc} **/
    @Override
    public void move() {
        if (canMove()) {
            if (!this.didFirstMovement) {
                this.didFirstMovement = true;
            } else if (this.canMoveAfterAttack) {
                this.movedAfterAttack = true;
                this.attackCount = this.possibleAttack;
            }
        }
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canMove() {
        return (!this.didFirstMovement && this.attackCount == 0)
                || (this.canMoveAfterAttack && !this.movedAfterAttack && this.attackCount != 0);
    }

    /** {@inheritDoc} **/
    @Override
    public void attack() {
        if (canAttack()) {
            if (!this.didFirstMovement) {
                this.didFirstMovement = true;
            }
            this.attackCount++;
            if (this.canMoveAfterAttack) {
                this.movedAfterAttack = false;
            }
        }
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canAttack() {
        return possibleAttack > attackCount;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean movesAfterKill() {
        return this.moveOnKill;
    }

    /** {@inheritDoc} **/
    @Override
    public void takeDamage(final int opponentsStrength) {
        if (this.hp < opponentsStrength) {
            hp = 0;
        }
        hp -= opponentsStrength;
    }

    /** {@inheritDoc} **/
    @Override
    public void reset() {
        this.didFirstMovement = false;
        if (this.canMoveAfterAttack) {
            this.movedAfterAttack = true;
        }
        this.attackCount = 0;
    }

    /** {@inheritDoc} **/
    @Override
    public Set<Ability> getAbilities() {
        return Collections.unmodifiableSet(this.abilities);
    }

    /** {@inheritDoc} **/
    @Override
    public void addAbility(final Ability ability) {
        if (!abilities.contains(ability)) {
            this.abilities.add(ability);
        }
    }

    /** {@inheritDoc} **/
    @Override
    public void removeAbility(final Ability ability) {
        if (abilities.contains(ability)) {
            this.abilities.remove(ability);
        }
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttackCount() {
        return this.attackCount;
    }

    @Override
    public void setAttackCount(int attackCount) {
        this.attackCount = attackCount;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean didFirstMovement() {
        return this.didFirstMovement;
    }

    @Override
    public void setDidFirstMove(boolean didFirstMove) {
        this.didFirstMovement = didFirstMove;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean movedAfterAttack() {
        return this.movedAfterAttack;
    }

    @Override
    public void setMoveAfterAttack(boolean moved) {
        this.movedAfterAttack = moved;
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer,Optional<String>> descriptionMap = new LinkedHashMap<>();
        if (unitType.getGenericUnitType().equals(GenericUnitType.HERO)){
            if (unitType.equals(UnitType.HERO_CLOSE_FIGHTER)){
                descriptionMap.put(R.string.unit_info_close_hero, Optional.empty());
            } else {
                descriptionMap.put(R.string.unit_info_distance_hero, Optional.empty());
            }
        }
        descriptionMap.put(R.string.unit_info_strength, Optional.of(Integer.toString(this.strength)));
        descriptionMap.put(R.string.unit_info_hp, Optional.of(this.hp + "/" + this.initialHp));
        descriptionMap.put(R.string.unit_info_movement_range, Optional.of(Integer.toString(this.movementRange)));
        descriptionMap.put(R.string.unit_info_attack_range, Optional.of(Integer.toString(this.attackRange)));
        descriptionMap.put(R.string.unit_info_remaining_attacks, Optional.of(Integer.toString(this.possibleAttack - this.attackCount)));
        descriptionMap.put(R.string.unit_info_can_move, Optional.of((canMove()+"")));
        descriptionMap.put(R.string.unit_info_can_move_after_attacking, Optional.of(this.canMoveAfterAttack+""));
        descriptionMap.put(R.string.unit_info_move_on_kill, Optional.of(this.moveOnKill+""));
        return descriptionMap;
    }

    /** {@inheritDoc} **/
    @Override
    public Cost getCost() {
        return this.unitCost;
    }

    /** {@inheritDoc} **/
    @Override
    public String getCostToString() {
        return this.unitCost.toString();

    }

    /** {@inheritDoc} **/
    @Override
    public Cost getUnlockCost() {
        return this.unitUnlockCost;
    }

    /** {@inheritDoc} **/
    @Override
    public String getUnlockCostToString() {
        return this.unitUnlockCost.toString();
    }

    /** {@inheritDoc} **/
    @Override
    public int getNameId() {
        return this.nameId;
    }

}
