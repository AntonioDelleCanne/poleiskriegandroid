package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The UnitLevel class extends LevelAttribute with Unit as generic type. This
 * class manage the unit level structure. If a user increase is unit level, he
 * unlock new unit.
 */
public class UnitLevel extends LevelAttribute<Unit> {

    private static final String ATTRIBUTE_DESCRIPTION = "Unit level increase: unlock ";
    private static final int ATTRIBUTE_NAME = R.string.skilltree_attribute_unit;
    private static final String WHITELIST_PACKAGE = "com.example.poleiskrieg.logics.game.model.objects.unit";
    private static final String CLASSES_IMPLEMENTING = "com.example.poleiskrieg.logics.game.model.objects.unit.Unit";
    private static final String FILTER_START = "com.example.poleiskrieg.logics.game.model.objects.unit.Level";
    private static final String FILTER_END = "Unit";
    private static final int INITIAL_LEVEL = 1;

    /**
     * UnitLevel constructor.
     */
    public UnitLevel() {
        super(INITIAL_LEVEL, WHITELIST_PACKAGE, CLASSES_IMPLEMENTING, FILTER_START, FILTER_END);
    }

    /** {@inheritDoc} **/
    @Override
    protected Optional<Unit> getObject(final int level) {
        return getUnitWithOwner(level, Optional.empty());
    }

    @Override
    public int getAttributeNameId() {
        return ATTRIBUTE_NAME;
    }

    /** {@inheritDoc} **/
    @Override
    public Cost getCost() {
        verify();
        return getNewObject().get().getUnlockCost();
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttributeLevelNameId() {
        return getNewObject().get().getNameId();
    }

    /**
     * This method can be use to get the list of all possible unit that a owner can create.
     * @param player is the player that own the SkillTree.
     * @return all possible unit that the player can create.
     */
    public List<Unit> getPossibleUnit(final Player player) {
        return IntStream.range(0, getCurrentValue() + 1).mapToObj(i -> getUnitWithOwner(i, Optional.of(player)).get())
                .collect(Collectors.toList());
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> clazz = Class.forName(getNewObject().get().getClass().getName());
        GameObject object;
        if(owner.isPresent()){
            Constructor<?> ctor = clazz.getConstructor(Player.class);
            object = (GameObject) ctor.newInstance(owner.get());
        } else {
            Constructor<?> ctor = clazz.getConstructor();
            object = (GameObject) ctor.newInstance();
        }
        return object.getDescription();
    }

    private Optional<Unit> getUnitWithOwner(final int level, final Optional<Player> owner) {
        try {
            if (owner.isPresent()) {
                return Optional.of((Unit) Class.forName(getObjectClasses().get(level)).getConstructor(Player.class)
                        .newInstance(owner.get()));
            } else {
                return Optional.of((Unit) Class.forName(getObjectClasses().get(level)).getConstructor().newInstance());
            }
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException | ClassNotFoundException | IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

}
