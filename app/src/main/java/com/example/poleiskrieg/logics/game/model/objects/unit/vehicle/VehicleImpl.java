package com.example.poleiskrieg.logics.game.model.objects.unit.vehicle;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.objects.unit.GenericUnitType;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.UnitImpl;
import com.example.poleiskrieg.logics.game.model.objects.unit.UnitType;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The VehicleImpl is an abstract class that implements Vehicle and extends
 * UnitImpl. A vehicle has attributes similar to the generic unit.
 * This class has the getPassengerTypesAccept abstract method.
 */
public abstract class VehicleImpl extends UnitImpl implements Vehicle {

    private static final UnitType TYPE = UnitType.WATER_VEHICLE;
    private Optional<Unit> passenger;

    /**
     * VehicleImpl constructor.
     * 
     * @param nameId             vehicle's name id.
     * @param strength           vehicle's strength.
     * @param initialHp          vehicle's initial life.
     * @param moveRange          vehicle's movement range.
     * @param attackRange        vehicle's attack range.
     * @param possibleAttack     vehicle's possible attack in a turn.
     * @param canMoveAfterAttack if the vehicle can move after he has attacked someone.
     * @param moveOnKill         if the vehicle kills the opponent, he moves in the
     *                           opponent's cell.
     * @param vehicleCost        vehicle's cost.
     * @param vehicleUnlockCost  vehicle's unlock cost. If empty is already unlock.
     * @param passenger          vehicle's passenger.
     * @param vehicleType        vehicle's type.
     */
    public VehicleImpl(final int nameId, final int strength, final int initialHp, final int moveRange,
                       final int attackRange, final int possibleAttack, final boolean canMoveAfterAttack, final boolean moveOnKill,
                       final Cost vehicleCost, final Cost vehicleUnlockCost, final Optional<Unit> passenger,
                       final UnitType vehicleType) {
        super(passenger.isPresent() ? passenger.get().getOwner() : Optional.empty(), nameId,
                passenger.map(unit -> strength + unit.getOwner().get().getRace().getStrBoost(TYPE)).orElse(strength),
                passenger.map(value -> initialHp + value.getOwner().get().getRace().getHpBoost(TYPE)).orElse(initialHp),
                passenger.map(unit1 -> moveRange + unit1.getOwner().get().getRace().getMovRangeBoost(TYPE)).orElse(moveRange),
                passenger.map(unit2 -> attackRange + unit2.getOwner().get().getRace().getAttRangeBoost(TYPE)).orElse(attackRange),
                passenger.map(unit3 -> possibleAttack + unit3.getOwner().get().getRace().getPossibleAttBoost(TYPE)).orElse(possibleAttack),
                canMoveAfterAttack, moveOnKill, vehicleCost, vehicleUnlockCost, vehicleType);
        this.passenger = passenger;
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Unit> getPassenger() {
        return this.passenger;
    }

    /** {@inheritDoc} **/
    @Override
    public abstract List<UnitType> getPassengerTypesAccept();

    @Override
    public void setPassenger(Unit passenger) {
        this.passenger = Optional.of(passenger);
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer,Optional<String>> descriptionMap = super.getDescription();
        passenger.ifPresent(unit -> {
            descriptionMap.put(R.string.vehicle_info_passenger_info, Optional.empty());
            descriptionMap.put(unit.getNameId(), Optional.empty());
        });
        return descriptionMap;
    }
}
