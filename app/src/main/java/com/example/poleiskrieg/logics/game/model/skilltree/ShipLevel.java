package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;


/**
 * The ShipLevel class extends LevelAttribute with Vehicle as generic type.
 * This class manage the ship level. The level of this class determines what
 * level the ships will be from the moment they are created.
 */
public class ShipLevel extends LevelAttribute<Vehicle> {

    private static final int ATTRIBUTE_DESCRIPTION = R.string.skilltree_attribute_upgrade_ship;
    private static final int ATTRIBUTE_NAME = R.string.skilltree_attribute_ship;
    private static final String WHITELIST_PACKAGE = "com.example.poleiskrieg.logics.game.model.objects.unit.vehicle";
    private static final String CLASSES_IMPLEMENTING = "com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle";
    private static final String FILTER_START = "com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Level";
    private static final String FILTER_END = "Ship";
    private static final int INITIAL_LEVEL = 0;

    /**
     * ShipLevel constructor.
     */
    public ShipLevel() {
        super(INITIAL_LEVEL, WHITELIST_PACKAGE, CLASSES_IMPLEMENTING, FILTER_START, FILTER_END);
    }

    /** {@inheritDoc} **/
    @Override
    protected Optional<Vehicle> getObject(final int level) {
        return getShipWithPassenger(level, Optional.empty());
    }

    private Optional<Vehicle> getShipWithPassenger(final int level, final Optional<Unit> passenger) {
        try {
            return Optional.of((Vehicle) Class.forName(getObjectClasses().get(level)).getConstructor(Optional.class)
                    .newInstance(passenger));
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException | ClassNotFoundException | IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(e);
            //return Optional.empty();
        }
    }

    /** {getObjectClasses().get(level)@inheritDoc} **/
    @Override
    public Cost getCost() {
        return this.getNewObject().get().getUnlockCost();
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttributeNameId() {
        return this.ATTRIBUTE_NAME;
    }

    /** {@inheritDoc} **/
    @Override
    public int getAttributeLevelNameId() {
        return getNewObject().get().getNameId();
    }

    /**
     * This method can be use to take the ship of the current level.
     * 
     * @param passenger is the passenger of the ship.
     * @return the actual level ship.
     */
    public Vehicle getActualShip(final Unit passenger) {
        return getShipWithPassenger(getCurrentValue(), Optional.of(passenger)).get();
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) {
        Map<Integer, Optional<String>> description = new LinkedHashMap<>();
        description.put(ATTRIBUTE_DESCRIPTION, Optional.of(Integer.toString(this.getCurrentValue()+2)));
        return description;
    }
}
