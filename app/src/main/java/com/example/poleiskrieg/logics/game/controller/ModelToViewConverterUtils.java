package com.example.poleiskrieg.logics.game.controller;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.example.poleiskrieg.logics.game.controller.selection.Selection;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.example.poleiskrieg.logics.game.model.resources.BasicResources;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.logics.game.model.skilltree.LevelAttribute;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;

/**
 * This class is package protected.
 * 
 * The images to be drawn in the view are retrieved from model object ID's in
 * the following way: the model objects are identified by an ID with the
 * following form: (Terrain/Pg),(specific Pg/Terrain class name); the selection
 * type is identified by its enum name and by the enum toString; resources to
 * draw are contained in the sprites folder which contains: -a pg folder; -a
 * terrain folder; -a selection folder. In each of them there are images named
 * after the class they represent whit the following form: ClassName ->
 * class_name
 * **
 * CHANGED TO
 * **
 * sprite_owner(terrain, selection, race,neutral)[_type]_class_name
 */
public final class ModelToViewConverterUtils {

    // info about the directory containing the sprites and the sprites file
    // extension
    private static final String URL_SEPARATOR = "_";
    private static final String SPRITES_PATH = "sprite_";
    private static final String ICON_RESOURCES_PATH = "icon_resource_";
    private static final String ICON_RACE_PATH = "icon_race_";
    private static final String ICON_RACE_SELECTED = "_selected";
    private static final String ICON_SKILLTREE_PATH = "skilltree_attribute_";
    private static final String LEVEL_STRING = "level";
    private static final String SPRITES_EXTENSION = "";
    private static final String SELECTION_PATH = "selection_";

    private ModelToViewConverterUtils() {
    }

    /**
     * This method can be used to take the id in string form.
     * @param from is the GameObject.
     * @return the id to string
     */
    public static String modelObjectToViewId(final GameObject from) {
        final StringBuilder result = new StringBuilder(SPRITES_PATH);
        if (from instanceof Terrain) {
            result.append("terrain_");
        } else {
            if (from.getOwner().isPresent()) {
                result.append(getSpacedFromCamelCase(from.getOwner().get().getRace().getClass().getSimpleName()));
                result.append(URL_SEPARATOR);
            } else {
                result.append("neutral_");
            }
            if (from instanceof Unit) {
                result.append(getSpacedFromCamelCase(Unit.class.getSimpleName()));
                result.append(URL_SEPARATOR);
            } else if (from instanceof Structure) {
                result.append(getSpacedFromCamelCase(Structure.class.getSimpleName()));
                result.append(URL_SEPARATOR);
            }
        }
        result.append(getSpacedFromCamelCase(from.getClass().getSimpleName()));
        result.append(SPRITES_EXTENSION);

        return result.toString();
    }

    /**
     * This method can be used to take the id in string form.
     * @param from is the Resource.
     * @return the id to string
     */
    public static String modelResourceToViewId(final Resource from) {
        final StringBuilder result = new StringBuilder(ICON_RESOURCES_PATH);
        if(from instanceof BasicResources){
            result.append(((BasicResources) from).name().toLowerCase());
        }
        return result.toString();
    }

    /**
     * This method can be used to take the id in string form.
     * @param from is the Resource.
     * @return the id to string
     */
    public static String modelAttributeToViewId(final SkillTreeAttribute from, Player owner) {
        final StringBuilder result = new StringBuilder();
        if(from instanceof LevelAttribute){
            GameObject obj = (GameObject) ((LevelAttribute) from).getNewObject().get();
            obj.set(Optional.of(owner));
            return modelObjectToViewId(obj);
        } else {
            result.append(ICON_SKILLTREE_PATH);
            result.append(getSpacedFromCamelCase(from.getClass().getSimpleName()));
            result.append(URL_SEPARATOR);
            result.append(LEVEL_STRING);
            result.append(from.getCurrentValue()+1);
        }
        return result.toString();
    }

    /**
     * This method can be used to take the id in string form.
     * @param from is the Selection.
     * @return the id to string
     */
    public static String modelSelectionToViewId(final Selection from) {
        final StringBuilder result = new StringBuilder(SPRITES_PATH);
        result.append(SELECTION_PATH);
        result.append(from.getId());
        result.append(SPRITES_EXTENSION);
        return result.toString();
    }

    /**
     * This method can be used to take the id in string form.
     * @param from is the Selection.
     * @return the id to string
     */
    public static String modelRaceToViewId(final Race from, boolean isSelected) {
        final StringBuilder result = new StringBuilder(ICON_RACE_PATH);
        result.append(getSpacedFromCamelCase(from.getClass().getSimpleName()));
        if(isSelected){
            result.append(ICON_RACE_SELECTED);
        }
        return result.toString();
    }

    /**
     * @param camelCase CamelCase
     * @return camel_case
     */
    static String getSpacedFromCamelCase(final String camelCase) {
        String res = Stream.of(camelCase.split("(?=\\p{Upper})")).collect(Collectors.joining("_")).toLowerCase();
        if(res.charAt(0) == '_') res = res.substring(1);
        return res;
    }
}
