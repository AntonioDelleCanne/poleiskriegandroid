package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Optional;

/**
 * The SkillTreeAttributeAbstract is an abstract class that implements
 * SkillTreeAttribute.
 */
public abstract class SkillTreeAttributeAbstract implements SkillTreeAttribute {

    private int currentValue;

    /**
     * SkillTreeAbstract constructor.
     * 
     * @param currentLevel is the initial value of the attribute.
     */
    public SkillTreeAttributeAbstract(final int currentLevel) {
        this.currentValue = currentLevel;
    }

    /** {@inheritDoc} **/
    @Override
    public void upgrade() {
        verify();
        this.currentValue++;
    }

    /** {@inheritDoc} **/
    @Override
    public String getCostToString() {
        return getCost().toString();
    }

    /** {@inheritDoc} **/
    @Override
    public abstract int getAttributeNameId();

    /** {@inheritDoc} **/
    @Override
    public abstract int getAttributeLevelNameId();

    /** {@inheritDoc} **/
    public abstract Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

    /** {@inheritDoc} **/
    @Override
    public abstract boolean canUpgrade();

    /** {@inheritDoc} **/
    @Override
    public abstract Cost getCost();

    /**
     * This method can be use for verify if isn't possible to upgrade the attribute.
     * 
     * @throws IllegalStateException if the attribute can't be upgrade.
     */
    protected void verify() {
        if (!canUpgrade()) {
            throw new IllegalStateException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCurrentValue() {
        return this.currentValue;
    }

    /**
     * This method can be use to set the current attribute level.
     * 
     * @param newValue is the new value for the attribute.
     */
    protected void setCurrentValue(final int newValue) {
        this.currentValue = newValue;

    }
}
