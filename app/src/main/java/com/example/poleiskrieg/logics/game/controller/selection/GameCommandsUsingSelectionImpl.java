package com.example.poleiskrieg.logics.game.controller.selection;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import android.app.Application;
import android.content.Context;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.Capital;
import com.example.poleiskrieg.logics.game.model.objects.structures.City;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;
import com.example.poleiskrieg.logics.game.util.Coordinates;

import com.example.poleiskrieg.logics.game.util.Pair;
import com.example.poleiskrieg.repositories.game.GameRepository;
import com.example.poleiskrieg.repositories.game.OnlineGameRepository;

/**
 * Implementation of GameCommandsUsingSelection.
 */
public class GameCommandsUsingSelectionImpl implements GameCommandsUsingSelection {

    private final GameCommandsWithSelection gameCommands;

    /**
     *
     * @param application
     * @param gameId id of the game to be loaded
     */
    public GameCommandsUsingSelectionImpl(Application application, long gameId) {
        this.gameCommands = new GameCommandsWithSelection(gameId, application);
    }


    public GameCommandsUsingSelectionImpl(GameRepository gameRepository) {
        this.gameCommands = new GameCommandsWithSelection(gameRepository);
    }

    /** {@inheritDoc} **/
    @Override
    public void selectPosition(final Coordinates cords) {
        this.gameCommands.selectPosition(cords);
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Selection> getCaseSelection(final Coordinates cords) {
        return this.gameCommands.getCaseSelection(cords);
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Pair<Coordinates, GameObject>> getActualSelection() {
        return this.gameCommands.getActualSelection();
    }

    /** {@inheritDoc} **/
    @Override
    public void select(final Coordinates cords, final GameObjectSelection selection) {
        this.gameCommands.select(cords, selection);
    }

    /** {@inheritDoc} **/
    @Override
    public Pair<Integer, Integer> getMapSize() {
        return this.gameCommands.getMapSize();
    }

    /** {@inheritDoc} **/
    @Override
    public Terrain getTerrain(final Coordinates cords) {
        return this.gameCommands.getTerrain(cords);
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Structure> getStructure(final Coordinates cords) {
        return this.gameCommands.getStructure(cords);
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Unit> getUnit(final Coordinates cords) {
        return this.gameCommands.getUnit(cords);
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Coordinates, List<GameObject>> toMap() {
        return this.gameCommands.toMap();
    }

    /** {@inheritDoc} **/
    @Override
    public List<Unit> getPossibleUnit() {
        return this.gameCommands.getPossibleUnit();
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getAttributeNewObjectDescription(int attributePosition) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return this.gameCommands.getAttributeNewObjectDescription(attributePosition);
    }

    /** {@inheritDoc} **/
    @Override
    public List<SkillTreeAttribute> getSkillTreeUpgradableAttribute() {
        return this.gameCommands.getSkillTreeUpgradableAttribute();
    }

    /** {@inheritDoc} **/
    @Override
    public void upgradeAttribute(final SkillTreeAttribute attribute) {
        this.gameCommands.upgradeAttribute(attribute);
    }

    /** {@inheritDoc} **/
    @Override
    public void nextTurn() {
        this.resetSelection();
        this.gameCommands.nextTurn();
    }

    /** {@inheritDoc} **/
    @Override
    public boolean isNextTurnWin(){
        return this.gameCommands.isNextTurnWin();
    }

    /** {@inheritDoc} **/
    @Override
    public void deleteGame() {
        this.gameCommands.deleteGame();
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Player> getWinnerPlayer() {
        return this.gameCommands.getWinnerPlayer();
    }

    /** {@inheritDoc} **/
    @Override
    public Player getCurrentPlayer() {
        return this.gameCommands.getCurrentPlayer();
    }

    /** {@inheritDoc} **/
    @Override
    public String getPlayerInfo() {
        return this.gameCommands.getPlayerInfo();
    }

    /** {@inheritDoc} **/
    @Override
    public void createUnitFromSelectedCity(final Unit unit) {
        if (isActualSelectionInstanceOfCity()) {
            this.gameCommands.createUnitFromCity(unit, this.getActualSelection().get().getKey());
        } else {
            throw new IllegalArgumentException();
        }
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canCreateUnit(final Unit unit) {
        return this.gameCommands.canCreateUnit(unit);
    }

    /** {@inheritDoc} **/
    @Override
    public boolean isHeroNotInGame(final Unit unit) {
        return this.gameCommands.isHeroNotInGame(unit);
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canSelectedCityCreate() {
        return isActualSelectionInstanceOfCity()
                && this.gameCommands.canCityCreate(this.getActualSelection().get().getKey());
    }
    
    /** {@inheritDoc} **/
    @Override
    public boolean canUpgradeAttribute(final SkillTreeAttribute attributeToUpgrade) {
        return this.gameCommands.canUpgradeAttribute(attributeToUpgrade);
    }

    private boolean isActualSelectionInstanceOfCity() {
        return this.getActualSelection().isPresent() && (this.getActualSelection().get().getValue() instanceof City);
    }

    /** {@inheritDoc} **/
    @Override
    public void resetSelection() {
        this.gameCommands.resetSelection();
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Resource, String> getCurrentPlayerResourcesDescriptionMap(){
        return this.gameCommands.getCurrentPlayerResourcesDescriptionMap();
    }

    /** {@inheritDoc} **/
    @Override
    public int getActualPlayerSpecificStructureCount(Class<? extends Structure> structureClass){
        return this.gameCommands.getActualPlayerSpecificStructureCount(structureClass);
    }

}
