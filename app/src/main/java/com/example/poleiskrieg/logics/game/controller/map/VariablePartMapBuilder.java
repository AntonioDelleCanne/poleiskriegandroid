package com.example.poleiskrieg.logics.game.controller.map;

import java.util.HashMap;
import java.util.Map;

import com.example.poleiskrieg.logics.game.util.Coordinates;
import com.example.poleiskrieg.logics.game.util.mapbuilder.AbstractGameMapBuilderUsingCaseBuilders;
import com.example.poleiskrieg.logics.game.util.mapbuilder.CaseBuilder;

import com.example.poleiskrieg.logics.game.util.Pair;

/**
 * builds only the variable part of the map. package protected.
 */
public class VariablePartMapBuilder
        extends AbstractGameMapBuilderUsingCaseBuilders<Map<Coordinates, VariableCasePart>, VariableCasePart> {

    public VariablePartMapBuilder(final Pair<Integer, Integer> size) {
        super(size, (Coordinates cords) -> new VariableCasePartBuilder());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Coordinates, VariableCasePart> build() {
        final Map<Coordinates, CaseBuilder<VariableCasePart>> mapToBuild = super.getMap();
        final Map<Coordinates, VariableCasePart> result = new HashMap<>();
        for (int i = 0; i < super.getSize().getKey(); i++) {
            for (int j = 0; j < super.getSize().getValue(); j++) {
                final Coordinates cords = new Coordinates(i, j);
                result.put(cords, mapToBuild.get(cords).build());
            }
        }
        return result;
    }

}
