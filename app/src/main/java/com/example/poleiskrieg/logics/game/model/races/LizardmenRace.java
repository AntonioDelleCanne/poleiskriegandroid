package com.example.poleiskrieg.logics.game.model.races;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.BasicCostImpl;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.objects.unit.UnitType;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class LizardmenRace extends RaceImpl {

    private static final int BASIC_CLOSE_FIGHTER_STR = +5;
    private static final int BASIC_CLOSE_FIGHTER_GOLD = +50;
    private static final int BASIC_CLOSE_FIGHTER_WOOD = -40;

    private static final int BASIC_DISTANCE_FIGHTER_STR = -5;
    private static final int BASIC_DISTANCE_FIGHTER_MOVE_RANGE = +1;
    private static final int BASIC_DISTANCE_FIGHTER_GOLD = +60;
    private static final int BASIC_DISTANCE_FIGHTER_WOOD = -20;

    private static final int NORMAL_CLOSE_FIGHTER_STR = -10;
    private static final int NORMAL_CLOSE_FIGHTER_HP = -5;
    private static final int NORMAL_CLOSE_FIGHTER_MOVE_RANGE = +2;
    private static final int NORMAL_CLOSE_FIGHTER_GOLD = +30;
    private static final int NORMAL_CLOSE_FIGHTER_WOOD = -75;

    private static final int NORMAL_DISTANCE_FIGHTER_STR = +15;
    private static final int NORMAL_DISTANCE_FIGHTER_HP = +20;
    private static final int NORMAL_DISTANCE_FIGHTER_MOVE_RANGE = +1;
    private static final int NORMAL_DISTANCE_FIGHTER_ATT_RANGE = -3;
    private static final int NORMAL_DISTANCE_FIGHTER_GOLD = +60;
    private static final int NORMAL_DISTANCE_FIGHTER_WOOD = -60;

    private static final int HERO_CLOSE_FIGHTER_HP = +30;
    private static final int HERO_CLOSE_FIGHTER_MOV_RANGE = +1;
    private static final int HERO_CLOSE_FIGHTER_GOLD = +100;

    private static final int HERO_DISTANCE_FIGHTER_MOV_RANGE = +2;
    private static final int HERO_DISTANCE_FIGHTER_ATT_RANGE = -2;
    private static final int HERO_DISTANCE_FIGHTER_POS_ATK = +2;
    private static final int HERO_DISTANCE_FIGHTER_GOLG = +50;
    private static final int HERO_DISTANCE_FIGHTER_WOOD = -100;

    private static final int WATER_VEHICLE_STR = +5;
    private static final int WATER_VEHICLE_MOV_RANGE = +1;
    private static final int WATER_VEHICLE_ATT_RANGE = -2;

    /**
     * Lizardmen race constructor.
     */
    public LizardmenRace() {
        super(R.string.lizardmen, R.string.lizardmen_description, getStatsBoostMap(), getCostBoostMap());
    }

    /**
     * This method is used to create the stats boost map. The order for the boost
     * is: strength boost, hp, movement range, attack range, possible attacks.
     *
     * @return boostMap the stats boost map.
     */
    private static Map<UnitType, List<Optional<Integer>>> getStatsBoostMap() {
        final Map<UnitType, List<Optional<Integer>>> boostMap = new LinkedHashMap<>();
        boostMap.put(UnitType.CLOSE_BASIC, Arrays.asList(Optional.of(BASIC_CLOSE_FIGHTER_STR),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));
        boostMap.put(UnitType.DISTANCE_BASIC,
                Arrays.asList(Optional.of(BASIC_DISTANCE_FIGHTER_STR), Optional.empty(),
                        Optional.of(BASIC_DISTANCE_FIGHTER_MOVE_RANGE), Optional.empty(), Optional.empty()));
        boostMap.put(UnitType.CLOSE_NORMAL, Arrays.asList(Optional.of(NORMAL_CLOSE_FIGHTER_STR),
                Optional.of(NORMAL_CLOSE_FIGHTER_HP), Optional.of(NORMAL_CLOSE_FIGHTER_MOVE_RANGE), Optional.empty(), Optional.empty()));
        boostMap.put(UnitType.DISTANCE_NORMAL, Arrays.asList(Optional.of(NORMAL_DISTANCE_FIGHTER_STR), Optional.of(NORMAL_DISTANCE_FIGHTER_HP),
                Optional.of(NORMAL_DISTANCE_FIGHTER_MOVE_RANGE), Optional.of(NORMAL_DISTANCE_FIGHTER_ATT_RANGE), Optional.empty()));
        boostMap.put(UnitType.HERO_CLOSE_FIGHTER,
                Arrays.asList(Optional.empty(), Optional.of(HERO_CLOSE_FIGHTER_HP),
                        Optional.of(HERO_CLOSE_FIGHTER_MOV_RANGE), Optional.empty(),
                        Optional.empty()));
        boostMap.put(UnitType.HERO_DISTANCE_FIGHTER, Arrays.asList(Optional.empty(),
                Optional.empty(), Optional.of(HERO_DISTANCE_FIGHTER_MOV_RANGE), Optional.of(HERO_DISTANCE_FIGHTER_ATT_RANGE), Optional.of(HERO_DISTANCE_FIGHTER_POS_ATK)));
        boostMap.put(UnitType.WATER_VEHICLE, Arrays.asList(Optional.of(WATER_VEHICLE_STR),
                Optional.empty(), Optional.of(WATER_VEHICLE_MOV_RANGE), Optional.of(WATER_VEHICLE_ATT_RANGE), Optional.empty()));
        return boostMap;
    }

    private static Map<UnitType, Cost> getCostBoostMap() {
        final Map<UnitType, Cost> costMap = new LinkedHashMap<>();
        costMap.put(UnitType.CLOSE_BASIC, new BasicCostImpl(Optional.of(BASIC_CLOSE_FIGHTER_GOLD),
                Optional.of(BASIC_CLOSE_FIGHTER_WOOD), Optional.empty()));
        costMap.put(UnitType.DISTANCE_BASIC, new BasicCostImpl(Optional.of(BASIC_DISTANCE_FIGHTER_GOLD),
                Optional.of(BASIC_DISTANCE_FIGHTER_WOOD), Optional.empty()));
        costMap.put(UnitType.CLOSE_NORMAL, new BasicCostImpl(Optional.of(NORMAL_CLOSE_FIGHTER_GOLD),
                Optional.of(NORMAL_CLOSE_FIGHTER_WOOD), Optional.empty()));
        costMap.put(UnitType.DISTANCE_NORMAL, new BasicCostImpl(Optional.of(NORMAL_DISTANCE_FIGHTER_GOLD),
                Optional.of(NORMAL_DISTANCE_FIGHTER_WOOD), Optional.empty()));
        costMap.put(UnitType.HERO_CLOSE_FIGHTER, new BasicCostImpl(Optional.of(HERO_CLOSE_FIGHTER_GOLD),
                Optional.empty(), Optional.empty()));
        costMap.put(UnitType.HERO_DISTANCE_FIGHTER,
                new BasicCostImpl(Optional.of(HERO_DISTANCE_FIGHTER_GOLG), Optional.of(HERO_DISTANCE_FIGHTER_WOOD), Optional.empty()));
        return costMap;
    }
}