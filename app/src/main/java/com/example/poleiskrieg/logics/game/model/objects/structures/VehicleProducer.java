package com.example.poleiskrieg.logics.game.model.objects.structures;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle;

/**
 * Models a Structure responsible of the production of vehicles.
 */
public interface VehicleProducer extends Structure {
    /**
     * @param unit the unit to put in the vehicle
     * 
     * @return the vehicle produced with the Unit inside
     */
    Vehicle getVehicle(Unit unit);

    void setSkillTreeManager(SkillTreeManager skillTreeManager);
}
