package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Optional;

/**
 * The SkillTreeAttribute interface represent an attribute of a SkillTree. A
 * SkillTreeAttributes has a name, a current level and can has a next level and
 * a cost of the next level. If a next level is present, the attribute can be
 * upgrade to this level.
 */
public interface SkillTreeAttribute {

    /**
     * This method could be use to get the attribute's name id.
     *
     * @return the attribute's name id.
     */
    int getAttributeNameId();

    /**
     * This method could be use to get the attribute level's name id.
     *
     * @return the attribute level's name id.
     */
    int getAttributeLevelNameId();

    /**
     * This method could be use to get the attribute's description.
     * 
     * @return the attribute's description map.
     */
    Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

    /**
     * This method can be use to upgrade the level of the attribute.
     */
    void upgrade();

    /**
     * This method can be use to verify if an attribute can be upgrade.
     * 
     * @return true if a next level of the attribute is present.
     */
    boolean canUpgrade();

    /**
     * This method can be used to get the upgrade's cost.
     * @return the upgrade's cost.
     */
    Cost getCost();

    /**
     * This method can be used to get the upgrade's cost to string version.
     * @return the upgrade's cost to string version.
     */
    String getCostToString();

    /**
     * This method can be use to get the current attribute level.
     *
     * @return the current level of the attribute
     */
    int getCurrentValue();

}
