package com.example.poleiskrieg.logics.game.model.player;

import androidx.annotation.Nullable;

import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.google.common.hash.HashCode;

/**
 * The player for the current game.
 */
public class PlayerImpl implements Player {

    private final String name;
    private final int id;
    private final Race race;
    private final Objective objective;

    /**
     * Initialize the player with its informations.
     * 
     * @param name      the name of the player
     * 
     * @param id        the unique identifier of the player
     * 
     * @param race      the race of the player
     * 
     * @param objective the objective of the player
     * 
     */
    public PlayerImpl(final String name, final int id, final Race race, final Objective objective) {
        this.name = name;
        this.id = id;
        this.race = race;
        this.objective = objective;
    }

    /** {@inheritDoc} **/
    @Override
    public String getName() {
        return this.name;
    }

    /** {@inheritDoc} **/
    @Override
    public int getId() {
        return this.id;
    }

    /** {@inheritDoc} **/
    @Override
    public Race getRace() {
        return this.race;
    }

    /** {@inheritDoc} **/
    @Override
    public Objective getObjective() {
        return this.objective;
    }

    /**
     * @return the description of the Player
     */
    public String toString() {
        return "Player " + this.id + "\nName: " + this.name + ", RaceId: " + this.race.getRaceNameId() + ", Resources:\n";
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Player player = (Player) obj;
        return this.getId() == player.getId();
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
