package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The CapitalImpl class implements Capital and extends CityImpl. The difference
 * of this class with CityImpl, in addition to the Capital methods it
 * implements, is the amount of population it produces, which will vary
 * according to the level of the capital
 */
public class CapitalImpl extends CityImpl implements Capital {

    private final int nameId;
    private final double enemyAttackReduction;
    private final int populationBoost;
    private final Cost unlockCost;

    /**
     * CapitalImpl constructor.
     * 
     * @param player               is the player that own the capital;
     * @param nameId               is the name id of the capital;
     * @param enemyAttackReduction the attack reduction that will be applied to
     *                             attacks that involve an unit that is defending
     *                             his capital;
     * @param populationBoost      it is a boost to be applied to the population
     *                             produced by the capital;
     * @param unlockCost           is the cost to unlock a new capital level.
     */
    public CapitalImpl(final Optional<Player> player, final int nameId, final double enemyAttackReduction,
                       final int populationBoost, final Cost unlockCost) {
        super(player);
        this.nameId = nameId;
        this.enemyAttackReduction = enemyAttackReduction;
        this.populationBoost = populationBoost;
        this.unlockCost = unlockCost;
    }

    /** {@inheritDoc} **/
    @Override
    public double getEnemyAttackReduction() {
        return this.enemyAttackReduction;
    }

    /** {@inheritDoc} **/
    @Override
    public int getProducedQuantity() {
        return POPULATION + populationBoost;
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer, Optional<String>> descriptionMap = new LinkedHashMap<>();
        descriptionMap.put(R.string.game_object_info_owner, Optional.of(getOwnerName()));
        if (getOwner().isPresent()) {
            descriptionMap.put(R.string.structure_info_population, Optional.of(Integer.toString(getProducedQuantity())));
            descriptionMap.put(R.string.structure_info_attack_reduction, Optional.of((100 - getEnemyAttackReduction() * 100) + "%"));
        }
        return descriptionMap;
    }

    /** {@inheritDoc} **/
    @Override
    public Cost getUnlockCost() {
        return this.unlockCost;
    }

    /** {@inheritDoc} **/
    @Override
    public String getUnlockCostToString() {
        return this.unlockCost.toString();
    }

    @Override
    public int getNameId() {
        return this.nameId;
    }
}
