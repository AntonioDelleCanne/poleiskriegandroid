package com.example.poleiskrieg.viewmodel.playerlist;

import android.app.Application;

import androidx.annotation.NonNull;

import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.player.PlayerImpl;
import com.example.poleiskrieg.repositories.playerlist.OnlinePlayerListRepository;
import com.example.poleiskrieg.util.C;

import java.util.Objects;

public class OnlinePlayerListViewModel extends AbstractPlayersListViewModel {

    private final OnlinePlayerListRepository repository;

    public OnlinePlayerListViewModel(@NonNull Application application, long gameId) {
        super(application, gameId);
        this.repository = new OnlinePlayerListRepository(application);
        super.setGameWithPlayers(this.repository.getGameWithPlayers(gameId));
    }

    @Override
    public long createGame() {
        return this.repository.createGame(super.gameId);
    }

    public long getOnlineId() {
        return this.repository.getGameWithPlayers(gameId).gameEntity.onlineId;
    }

    public long getPlayerIdInGame() {
        return repository.getGameWithPlayers(gameId).playerEntityList.stream().filter(x -> x.playerAccountId.equals(C.getLoggedPlayerUsername().get())).findFirst().get().id;
    }

    @Override
    public boolean canAddThePlayer(int id, String name, String className) {
        if (players.stream().map(p -> p.getName()).anyMatch(n -> n.equals(name))
                || players.stream().map(p -> p.getRace().getClass().getSimpleName()).anyMatch(n -> n.equals(className))) {
            return false;
        }
        Player player = new PlayerImpl(name, id,
                getRaceList().stream().filter(r -> r.getClass().getSimpleName().equals(className)).findFirst().get(),
                selectedGameMode.generateObjective());
        this.players.add(player);
        this.repository.addPlayer(player, gameId);
        return true;
    }
}
