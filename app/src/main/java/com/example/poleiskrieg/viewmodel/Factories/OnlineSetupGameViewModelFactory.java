package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.setupgameviewmodel.OnlineSetupGameViewModel;

public class OnlineSetupGameViewModelFactory implements ViewModelProvider.Factory {

    private Application application;
    private EditText playerNameEditText;

    public OnlineSetupGameViewModelFactory(Application a, EditText e){
        this.application = a;
        this.playerNameEditText = e;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OnlineSetupGameViewModel(application, playerNameEditText);
    }
}
