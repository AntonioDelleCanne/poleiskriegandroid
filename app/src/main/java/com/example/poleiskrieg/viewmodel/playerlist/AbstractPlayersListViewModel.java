package com.example.poleiskrieg.viewmodel.playerlist;

import android.app.Application;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.player.PlayerImpl;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.example.poleiskrieg.repositories.playerlist.PlayerListRepository;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class AbstractPlayersListViewModel extends AndroidViewModel implements  PlayerListViewModelInterface {
    protected static final String PLAYER_NAME_INCIPIT = "Player%s";

    protected GameRules selectedGameMode;
    protected int numberOfPlayers;
    protected int selectedPosition;
    protected List<Player> players;
    protected List<Race> races;
    protected MutableLiveData<Boolean> isSelectionMenuVisible;
    protected EditText playerNameEditText;
    protected final long gameId;

    public AbstractPlayersListViewModel(@NonNull Application application, long gameId) {
        super(application);
        this.gameId = gameId;

        this.selectedPosition = 0;
        this.races = getRaceList();
        this.isSelectionMenuVisible = new MutableLiveData<>();
        this.isSelectionMenuVisible.setValue(false);
        //this.playerNameEditText = playerNameEditText;
        //playerNameEditText.setText(String.format(PLAYER_NAME_INCIPIT, getActualPlayerId()));
    }

    protected void setGameWithPlayers(GameWithPlayers gameWithPlayers){
        this.players = gameWithPlayers.playerEntityList.stream().map(e -> new PlayerImpl(e.name, e.id, e.race, e.objective)).collect(Collectors.toList());
        this.numberOfPlayers =  gameWithPlayers.gameEntity.numberOfPlayers;
        this.selectedGameMode = gameWithPlayers.gameEntity.gameRules;
    }

    public void setPlayerNameEditText(EditText playerNameEditText){
        this.playerNameEditText = playerNameEditText;
        this.playerNameEditText.setText(String.format(PLAYER_NAME_INCIPIT, getActualPlayerId()));
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public MutableLiveData<Boolean> getIsSelectionMenuVisible(){
        return this.isSelectionMenuVisible;
    }

    public void setIsSelectionMenuVisible(final boolean isVisible){
        this.isSelectionMenuVisible.setValue(isVisible);
    }

    public int getNumberOfPlayers(){
        return this.numberOfPlayers;
    }

    public long getGameId(){
        return this.gameId;
    }

    public GameRules getSelectedGameMode(){
        return this.selectedGameMode;
    }

    public abstract long createGame();

    /**
     * @return the number of players.
     */
    public int getPlayerNumber() {
        return this.numberOfPlayers;
    }

    /**
     * @return the list of players declared.
     */
    public List<Player> getPlayers() {
        return Collections.unmodifiableList(this.players);
    }

    /**
     * @return the actual player id.
     */
    public int getActualPlayerId() {
        //return getPlayers().isEmpty() ? 1 : IntStream.range(1, getPlayers().size() + 1).filter(x ->
                   // !getPlayers().stream().map(Player::getId).collect(Collectors.toList()).contains(x)).min().getAsInt();
        return getPlayers().size() + 1;
    }

    public boolean needPlayer(){
        return this.getActualPlayerId() <= this.numberOfPlayers;
    }

    public EditText getPlayerNameEditText(){return this.playerNameEditText;}

    /**
     * @return true if the conditions to get the game to start are all met.
     */
    public boolean canStart() {
        return this.players.size() == numberOfPlayers;
    }

    /**
     * Method that checks if the player can be added, and if it can then adds it.
     * The player will not be added if he has a race and / or a name already
     * selected by another player.
     *
     * @param id            is the player id.
     * @param name          is the name of the player to add.
     * @param className     is the race of the player to add.
     * @return true if the player has been added.
     */
    public abstract boolean canAddThePlayer(final int id, final String name, final String className);

    public Integer getRaceImageId(Race race){
        return C.getResId(ModelToViewConverterUtils.modelRaceToViewId(race,isRaceSelected(race)), R.drawable.class);
    }

    public Boolean isRaceAlreadySelected(){
        return isRaceSelected(getRaceList().get(getSelectedPosition()));
    }

    /**
     * @return the list with all the races available within the game.
     */
    public List<Race> getRaceList() {
        final List<Race> racesList = new LinkedList<>();
        ClassScan.get().getRaceClasses().forEach(s -> {
            try {
                if(!s.equals("com.example.poleiskrieg.logics.game.model.races.RaceImpl")){
                    racesList.add((Race) Class.forName(s).newInstance());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return Collections.unmodifiableList(racesList);
    }

    private Boolean isRaceSelected(Race race){
        return this.players.stream().map(p -> p.getRace()).anyMatch(p -> p.getClass().equals(race.getClass()));
    }
}
