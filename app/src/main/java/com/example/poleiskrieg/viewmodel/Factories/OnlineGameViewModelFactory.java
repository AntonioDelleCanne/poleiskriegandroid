package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.logics.game.controller.selection.GameCommandsUsingSelection;
import com.example.poleiskrieg.logics.game.controller.selection.GameCommandsUsingSelectionImpl;
import com.example.poleiskrieg.repositories.game.GameRepository;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class OnlineGameViewModelFactory implements ViewModelProvider.Factory{
    private Application mApplication;
    private GameCommandsUsingSelection game;
    private int playerId;

    public OnlineGameViewModelFactory(Application application, GameRepository gameRepository, int playerId) {
        this.mApplication = application;
        this.game = new GameCommandsUsingSelectionImpl(gameRepository);
        this.playerId = playerId;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new GameViewModel(mApplication, game, playerId);
    }
}
