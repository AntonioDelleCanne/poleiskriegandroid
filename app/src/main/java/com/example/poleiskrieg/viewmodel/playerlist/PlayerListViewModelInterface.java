package com.example.poleiskrieg.viewmodel.playerlist;

import android.widget.EditText;

import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.races.Race;

import java.util.List;

public interface PlayerListViewModelInterface {
    void setPlayerNameEditText(EditText playerNameEditText);

    int getSelectedPosition();

    void setIsSelectionMenuVisible(final boolean isVisible);

    MutableLiveData<Boolean> getIsSelectionMenuVisible();

    long getOnlineId();

    long getPlayerIdInGame();

    int getNumberOfPlayers();

    int getPlayerNumber();

    boolean needPlayer();

    EditText getPlayerNameEditText();

    boolean canStart();

    long createGame();

    long getGameId();

    boolean canAddThePlayer(final int id, final String name, final String className);

    Boolean isRaceAlreadySelected();

    GameRules getSelectedGameMode();

    public void setSelectedPosition(int selectedPosition);

    int getActualPlayerId();

    Integer getRaceImageId(Race race);

    List<Race> getRaceList();
}
