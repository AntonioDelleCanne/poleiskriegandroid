package com.example.poleiskrieg.viewmodel.setupgameviewmodel;

import android.app.Application;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.poleiskrieg.repositories.setupgame.OnlineSetupGameRepository;

public class OnlineSetupGameViewModel extends OfflineSetupGameViewModel {

    private final OnlineSetupGameRepository onlineRepository;

    public OnlineSetupGameViewModel(@NonNull Application application, EditText gameNameEditText) {
        super(application, gameNameEditText);
        this.onlineRepository = new OnlineSetupGameRepository(application);
    }

    @Override
    public LiveData<Long> createGame(){
        return this.onlineRepository.createOnlineGame(super.getGameNameEditText().getText().toString(), super.getPlayersNum().getValue(), super.getGameMode().getValue());
    }
}
