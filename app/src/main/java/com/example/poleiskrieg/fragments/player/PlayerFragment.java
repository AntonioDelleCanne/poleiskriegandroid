package com.example.poleiskrieg.fragments.player;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;
import com.example.poleiskrieg.viewmodel.playerlist.PlayersListViewModel;

public class PlayerFragment extends AbstractPlayerFragment {

    @Override
    public PlayerListViewModelInterface setViewModel() {
        return new ViewModelProvider(requireActivity()).get(PlayersListViewModel.class);
    }
}
