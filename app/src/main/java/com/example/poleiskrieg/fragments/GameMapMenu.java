package com.example.poleiskrieg.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.MainActivity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class GameMapMenu extends Fragment {

    private GameViewModel model;
    private TutorialViewModel tutorialViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_gamemap_menu, container, false);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        this.tutorialViewModel = new ViewModelProvider(requireActivity()).get(TutorialViewModel.class);
        TextView orientationText = view.findViewById(R.id.orientationScreenBtnText);
        LiveData<Boolean> isMenuVisible = model.getIsGameMenuVisible();
        isMenuVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        LiveData<Integer> orientation = model.getOrientationScreenIndex();
        orientation.observe(this.getViewLifecycleOwner(), bool -> {
            orientationText.setText(this.model.getOrientationScreenMode());
            switch (this.model.getOrientationScreenMode()){
                case R.string.screen_portrait:
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case R.string.screen_landscape:
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                default:
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

            }
        });
        setUI(view);
    }

    private void setUI(View view){
        view.findViewById(R.id.leftOrientationBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.setOrientationScreenIndex(false);
        });
        view.findViewById(R.id.rightOrintationBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.setOrientationScreenIndex(true);
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsGameMenuVisible(false);
        });
        view.findViewById(R.id.backButton).setOnClickListener(v -> {
            Intent intent = new Intent(view.getContext(), MainActivity.class);
            view.getContext().startActivity(intent);
        });
        view.findViewById(R.id.tutorialButton).setOnClickListener(v -> {
            this.tutorialViewModel.setIsTutorialVisible(true);
        });
    }
}