package com.example.poleiskrieg.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialMenuFragment extends Fragment {

    private TutorialViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorial_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(TutorialViewModel.class);
        model.getIsTutorialMenuVisible().observe(this.getViewLifecycleOwner(), bool -> {
            view.setVisibility(bool ? View.VISIBLE : View.INVISIBLE);
        });
        view.findViewById(R.id.rightBtn).setOnClickListener(v -> {
            model.changeInfo(true);
        });
        view.findViewById(R.id.leftBtn).setOnClickListener(v -> {
            model.changeInfo(false);
        });
        this.setTexts(view);
    }

    private void setTexts(View view) {
        model.getTutorialTitleText().observe(this.getViewLifecycleOwner(), string -> {
            ((TextView) view.findViewById(R.id.tutorialTitleText)).setText(string);
        });
        model.getSelectedImage().observe(this.getViewLifecycleOwner(), i -> {
            if (i > 10) {
                ((ImageView) view.findViewById(R.id.infoImage)).setImageResource(i);
            } else {
                ((ImageView) view.findViewById(R.id.infoImage)).setImageBitmap(null);
            }
        });
        model.getSelectedInfo().observe(this.getViewLifecycleOwner(), s -> {
            ((TextView) view.findViewById(R.id.selectedInfo)).setText(s);
            view.findViewById(R.id.rightBtn).setVisibility(model.getInfo().indexOf(s) + 1 == model.getInfo().size() ? View.INVISIBLE : View.VISIBLE);
            view.findViewById(R.id.leftBtn).setVisibility(model.getInfo().indexOf(s) == 0 ? View.INVISIBLE : View.VISIBLE);
        });
        model.getSelectedInfoText().observe(this.getViewLifecycleOwner(), s -> {
            ((TextView) view.findViewById(R.id.infoDescription)).setText(s);
        });
    }

}
