package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class PlayerInfoFragment extends Fragment{

    private static final int MENU_BUTTON_INDEX = 0;
    private static final int SKILLTREE_BUTTON_INDEX = 1;
    private static final int NEXTTURN_BUTTON_INDEX = 2;

    private GameViewModel model;
    private TextView playerName;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_gamemap_player_info, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        /*LiveData<Player> actualPlayer = model.getCurrentPlayer();
        actualPlayer.observe(this.getViewLifecycleOwner(), player -> {
            this.playerName = view.findViewById(R.id.playerNameText);
            playerName.setText(player.getName());
        });*/
        LiveData<Boolean> isVisible = model.getIsOnPlayerInfoVisible();
        isVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });

        LiveData<Boolean> isNextTurnWin = model.getIsNextTurnWin();
        isNextTurnWin.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                ButtonWithFeeling nextButton = view.findViewById(R.id.turnButton);
                nextButton.setText(R.string.player_game_menu_win);
                nextButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.icon_playermenu_win, 0, 0);
            }
        });

        view.findViewById(R.id.menuButton).setOnClickListener(v -> {
            this.model.setIsGameMenuVisible(true);
        });

        view.findViewById(R.id.infoButton).setOnClickListener(v -> {
            this.model.setInfoTitle(this.model.getActualPlayerName());
            this.model.setInfoDescription(this.model.getActualPlayerInfo());
            this.model.setIsOnInfoVisible(true);
        });

        view.findViewById(R.id.skilltreeButton).setOnClickListener(v -> {
            this.model.setIsOnSkilltreeVisible(true);
        });

        view.findViewById(R.id.turnButton).setOnClickListener(v -> {
            this.model.nextTurn();
            if (this.model.getIsNextTurnWin().getValue()){
                this.model.setWinnerInfoVisible(true);
            } else {
                this.model.setIsStartTurnInfoVisible(true);
            }
        });

    }
}