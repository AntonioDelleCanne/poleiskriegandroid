package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class DescriptionFragment extends Fragment {

    private GameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dialog_title_text, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        TextView title = view.findViewById(R.id.dialogTitleText);
        TextView description = view.findViewById(R.id.messageText);

        LiveData<Boolean> isOnInfoVisible = model.getIsOnInfoVisible();
        isOnInfoVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });

        LiveData<String> titleText = model.getInfoTitle();
        titleText.observe(this.getViewLifecycleOwner(), bool -> {
            title.setText(titleText.getValue());
        });

        LiveData<SpannableStringBuilder> descriptionText = model.getInfoDescription();
        descriptionText.observe(this.getViewLifecycleOwner(), bool -> {
            description.setText(descriptionText.getValue());
        });

        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsOnInfoVisible(false);
        });
    }
}