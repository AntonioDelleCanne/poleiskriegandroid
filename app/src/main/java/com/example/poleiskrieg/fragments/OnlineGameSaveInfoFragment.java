package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

public class OnlineGameSaveInfoFragment extends Fragment {

    private OnlineLoadGameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dialog_title_text, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(OnlineLoadGameViewModel.class);
        TextView title = view.findViewById(R.id.dialogTitleText);
        TextView msg = view.findViewById(R.id.messageText);
        LiveData<Boolean> isInfoVisible = model.getIsInfoVisible();
        isInfoVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                title.setText(this.model.getGameName());
                msg.setText(this.model.getDescriptionMsg());
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsInfoVisible(false);
        });
    }
}
