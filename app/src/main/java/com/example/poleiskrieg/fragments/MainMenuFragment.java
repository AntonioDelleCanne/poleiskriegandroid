package com.example.poleiskrieg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.LoadGameActivity;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.MainViewModel;

import java.util.Optional;

public class MainMenuFragment extends Fragment {

    private MainViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        view.findViewById(R.id.localPlayBtn).setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), LoadGameActivity.class);
            v.getContext().startActivity(intent);});

        view.findViewById(R.id.onlinePlayBtn).setOnClickListener(v -> {
            if(C.getLoggedPlayerUsername().isPresent()) {
                Intent intent = new Intent(v.getContext(), OnlineLoadGameActivity.class);
                v.getContext().startActivity(intent);
            }else {
                Toast.makeText(getContext(), R.string.need_log_in, Toast.LENGTH_LONG).show();
            }
        });

        view.findViewById(R.id.creditsBtn).setOnClickListener(v->{
            this.model.setIsCreditsVisible(true);
        });

        view.findViewById(R.id.settingsBtn).setOnClickListener(v->{
            this.model.setIsSettingsVisible(true);
        });

        view.findViewById(R.id.signInButton).setOnClickListener(v -> {
            this.model.setIsSignInVisible(true);
        });

        view.findViewById(R.id.signOutButton).setOnClickListener(v -> {
            C.setLoggedPLayer(Optional.empty());
            this.model.logOut();
            this.model.setIsLoggedIn(false);
            updateUI();
        });

        model.getIsLoggedIn().observe(this.getViewLifecycleOwner(), aBoolean -> {
            updateUI();
        });
        updateUI();
    }

    private void updateUI() {
        getActivity().findViewById(R.id.signInButton).setVisibility(!C.getLoggedPlayerUsername().isPresent() ? View.VISIBLE : View.INVISIBLE);
        getActivity().findViewById(R.id.signOutButton).setVisibility(!C.getLoggedPlayerUsername().isPresent() ? View.INVISIBLE : View.VISIBLE);
    }

}