package com.example.poleiskrieg.repositories;

import android.app.Application;
import android.util.Log;

import com.example.poleiskrieg.database.AppDatabase;
import com.example.poleiskrieg.util.C;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public class DatabaseRepository {

    protected final AppDatabase db;
    //add a server that observes live data for the online mode

    public DatabaseRepository(Application application) {
        this.db = AppDatabase.getDatabase(application);
    }

    public AppDatabase getDatabase(){
        return db;
    }

    public void executeOnDatabase(Runnable function){
        AppDatabase.databaseWriteExecutor.execute(function);
    }

    public <T> T submitOnDatabase(Callable<T> function){
        Future<T> future;
        T result = null;
        try {
            future = AppDatabase.databaseWriteExecutor.submit(function);
            result = future.get();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        if(result == null) {
            throw new IllegalStateException();
        }
        return result;
    }

}
