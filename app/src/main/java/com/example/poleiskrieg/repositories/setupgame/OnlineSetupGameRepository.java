package com.example.poleiskrieg.repositories.setupgame;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.database.converters.TypeConverters;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.Callable;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entitiesToJsonArray;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entityToJsonObject;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonArrayToEntities;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonObjectToEntity;

public class OnlineSetupGameRepository extends SetupGameRepository {

    private NetworkManager networkManager;

    public OnlineSetupGameRepository(Application application) {
        super(application);
        this.networkManager = networkManager.getInstance(application);
    }

    public LiveData<Long> createOnlineGame(String gameName, int numberOfPLayers, GameRules gameRules) {
        MutableLiveData<Long> gameId = new MutableLiveData<>();
        gameId.setValue((long) -1);

        JSONArray postParameters = new JSONArray();
        GameEntity gameEntity = getSetupGameEntity(gameName, numberOfPLayers, gameRules);
        postParameters.put(entityToJsonObject(gameEntity));
        Log.d(C.DEBUG_TAG, String.valueOf(postParameters));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.CREATE_GAME_ADDRESS,
                postParameters,
                response -> {
                    try {
                        JSONObject result = response.getJSONObject(0);
                        GameEntity gameEntityWithOnlineId = jsonObjectToEntity(GameEntity.class, result);
                        gameId.setValue(submitOnDatabase(() -> db.getGameDao().insert(gameEntityWithOnlineId)));
                        Log.d(C.DEBUG_TAG, response.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
        return gameId;
    }
}
