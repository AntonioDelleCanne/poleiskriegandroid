package com.example.poleiskrieg.repositories.game;

import androidx.lifecycle.LiveData;

import com.example.poleiskrieg.database.pojos.GameInfo;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.ResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.List;

public interface GameRepository {

    LiveData<GameInfo> getSavedGameData();

    void saveGame(GameInfo gameInfo);

    void saveGame(TurnManager turnManager, SkillTreeManager skillTreesManager, ResourceManager resources, ModifiableGameMap map, GameRules rules, boolean firstTurn, List<Player> players, String gameName);

    void deleteGame();

}
