package com.example.poleiskrieg.repositories.basegame;

import android.app.Application;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.ResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.Callable;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entitiesToJsonArray;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entityToJsonObject;

public class OnlineBaseGameRepository extends BaseGameRepository {
    protected final NetworkManager networkManager;

    public OnlineBaseGameRepository(Application application) {
        super(application);
        this.networkManager = NetworkManager.getInstance(application);
    }

    public void deleteGame(long gameId, long onlineId) {
        super.deleteGame(gameId);
        JSONObject id = new JSONObject();
        JSONArray postParameters = new JSONArray();
        try {
            id.put("onlineId", onlineId);
            postParameters.put(0, onlineId);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.DELETE_GAME_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
    }
}
