package com.example.poleiskrieg.repositories.game;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.database.pojos.GameInfo;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.ResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.repositories.basegame.BaseGameRepository;

import java.util.List;

public class OfflineGameRepository extends BaseGameRepository implements GameRepository{
    protected final long gameId;

    public OfflineGameRepository(Application application, long gameId) {
        super(application);
        this.gameId = gameId;
    }

    @Override
    public LiveData<GameInfo> getSavedGameData() {
        return new MutableLiveData<>(getSavedGameData(this.gameId));
    }

    @Override
    public void saveGame(GameInfo gameInfo) {
        saveGame(gameInfo.turnManager, gameInfo.skillTreesManager, gameInfo.resources, gameInfo.map, gameInfo.rules, gameInfo.firstTurn, gameInfo.players, gameInfo.gameName);
    }

    @Override
    public void saveGame(TurnManager turnManager, SkillTreeManager skillTreesManager, ResourceManager resources, ModifiableGameMap map, GameRules rules, boolean firstTurn, List<Player> players, String gameName) {
        saveGame(this.gameId, turnManager, skillTreesManager, resources, map, rules, firstTurn, players, gameName);
    }

    @Override
    public void deleteGame() {
        deleteGame(this.gameId);
    }
}
