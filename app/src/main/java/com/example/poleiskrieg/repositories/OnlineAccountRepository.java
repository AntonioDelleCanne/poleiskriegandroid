package com.example.poleiskrieg.repositories;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.database.AppDatabase;
import com.example.poleiskrieg.database.tables.AccountEntity;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entityToJsonObject;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonObjectToEntity;

public class OnlineAccountRepository extends DatabaseRepository {
    private final NetworkManager networkManager;

    public OnlineAccountRepository(Application application) {
        super(application);
        this.networkManager = NetworkManager.getInstance(application);
    }

    public LiveData<Boolean> logAccount(String name, String password) {
        MutableLiveData<Boolean> success = new MutableLiveData<>();
        JSONArray postParameters = new JSONArray();
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.username = name;
        accountEntity.password = password;
        postParameters.put(entityToJsonObject(accountEntity));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.LOG_IN_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, "response: " + response);

                    JSONObject returned = null;
                    try {
                        returned = response.getJSONObject(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(C.DEBUG_TAG, "returned: " + returned);
                   /* AccountEntity accountEntity = new AccountEntity();
                    accountEntity.username = name;
                    accountEntity.password = password;*/
                    try {
                        if(returned.getInt("result") == -1) {
                            success.setValue(false);
                        } else {
                            success.setValue(true);
                            addAccountToDatabase(accountEntity);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));
        networkManager.addToRequestQueue(request);
        return success;
    }

    public LiveData<Boolean> registerAccount(String name, String password) {
        MutableLiveData<Boolean> success = new MutableLiveData<>();
        JSONArray postParameters = new JSONArray();
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.username = name;
        accountEntity.password = password;
        postParameters.put(entityToJsonObject(accountEntity));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.ADD_ACCOUNT_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, "response: " + response);

                    JSONObject returned = null;
                    try {
                        returned = response.getJSONObject(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(C.DEBUG_TAG, "returned: " + returned);
                    try {
                        if(returned.getInt("result") == -1) {
                            success.setValue(false);
                        } else {
                            success.setValue(true);
                            addAccountToDatabase(accountEntity);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));
        networkManager.addToRequestQueue(request);
        return success;
    }

    public void logOut() {
        AccountEntity accountEntity = submitOnDatabase(() -> db.getAccountDao().getLoggedPlayer());
        executeOnDatabase(() -> db.getAccountDao().delete(accountEntity));
    }

    private void addAccountToDatabase(AccountEntity account) {
        executeOnDatabase(() -> db.getAccountDao().insert(account));
    }

}
